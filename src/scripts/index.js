import '../styles/index.scss';

// searchBar

$.widget("ui.autocomplete", $.ui.autocomplete, {

  _renderMenu: function(ul, items) {
    var that = this;
    ul.attr("class", "nav nav-pills nav-stacked  bs-autocomplete-menu");
    $.each(items, function(index, item) {
      that._renderItemData(ul, item);
    });
  },

  _resizeMenu: function() {
    var ul = this.menu.element;
    ul.outerWidth(Math.min(
      // Firefox wraps long text (possibly a rounding bug)
      // so we add 1px to avoid the wrapping (#7513)
      ul.width("").outerWidth() + 1,
      this.element.outerWidth()
    ));
  }

});

(function() {
  "use strict";
  var cities = [{
    "id": 1,
    "cityName": "Lyon"
  }, {
    "id": 2,
    "cityName": "La maison bleue, coteaux des lyonnais, chambre d'hôte, Lyon"
  }];

  $('.bs-autocomplete').each(function() {
    var _this = $(this),
      _data = _this.data(),
      _hidden_field = $('#' + _data.hidden_field_id);

    _this.after('<div class="bs-autocomplete-feedback form-control-feedback"><div class="loader">Loading...</div></div>')
      .parent('.form-group').addClass('has-feedback');

    var feedback_icon = _this.next('.bs-autocomplete-feedback');
    feedback_icon.hide();

    _this.autocomplete({
        minLength: 2,
        autoFocus: true,

        source: function(request, response) {
          var _regexp = new RegExp(request.term, 'i');
          var data = cities.filter(function(item) {
            return item.cityName.match(_regexp);
          });
          response(data);
        },

        search: function() {
          feedback_icon.show();
          _hidden_field.val('');
        },

        response: function() {
          feedback_icon.hide();
        },

        focus: function(event, ui) {
          _this.val(ui.item[_data.item_label]);
          event.preventDefault();
        },

        select: function(event, ui) {
          _this.val(ui.item[_data.item_label]);
          _hidden_field.val(ui.item[_data.item_id]);
          event.preventDefault();
        }
      })
      .data('ui-autocomplete')._renderItem = function(ul, item) {
        return $('<li></li>')
          .data("item.autocomplete", item)
          .append('<a>' + item[_data.item_label] + '</a>')
          .appendTo(ul);
      };
    // end autocomplete
  });
})();

// Slider

$(document).ready(function(){
  $('.slider').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    arrow: false, // because I custom arrow
  });
});

// Arrow custom

$('.left').click(function(){
  $('.slider').slick('slickPrev');
});

$('.right').click(function(){
  $('.slider').slick('slickNext');
});

// Agenda

$('.button').click(function(){
  $('.btn-date').css({"background-color": "#00A78A", "color": "white"});
});

$('.btn-date').click(function() {
  $('.calendarBox .container').toggle();
});

$('.calendar').datepicker({
  inline:true,
  firstDay: 1,
  showOtherMonths:true,
  dayNamesMin:['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
});
